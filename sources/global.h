#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <time.h>

#define BUF_SIZE 8192
#define FILENAME "meteo.txt"
#define MSG_SIZE 100
#define AREAS 3

typedef int bool;
#define true 1
#define false 0

/* Global functions prototypes */

char* mmapFile(mode_t, signed int);
