#include "global.h"

#define SERVER "meteo : "
#define UPTIME 30

int cli();
bool uATIS(char*);
bool pATIS(char*);
void alrmHandler(int);
void stopServer(bool*, char*, int);

static char ATIS[][MSG_SIZE] =
{
    "ATIS 1ONE EBLG 1803 00000KT 0600 FG OVC008 BKN040 PROB40 2024 0300 DZ FG OVC002 BKN040             \n",
    "ATIS 2TOW EBBR 0615 20015KT 80RA 42 ILLUMI UFOUFO NATI 4208 5000 RA BKN005 BECMG 0810 NSW BKN025   \n",
    "ATIS 3TRHE METAR VHHH 231830Z 06008KT 7000 FEW010SCT022 20/17 Q1017 NOSIG 5000 RA BKN005           \n",
    "ATIS 4FRUO 20015KT 8000 RA SCT010 OVC015 2024 0300 DZ FG 0810 9999 NSW BKN025                      \n",
    "ATIS 5VEFI KT 7000 FEW010SCT02 REKT 0608 5000 RA BKN005 EMPO 0608 5000 RA BKN005                   \n"
};

/* update is a value subject to change at any time hence the "volatile" */
volatile bool update;

int main (void)
{
	      bool running;

/* MMAP INITIALISATION */

	      int pid;
        char *pMap;

/* TODO - TIMER INTITIALISATON */
        struct sigaction sa;
        sa.sa_handler = alrmHandler;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        if(sigaction(SIGALRM, &sa, NULL) == -1)
          perror("sigaction");

        pMap = mmapFile(O_RDWR, PROT_READ | PROT_WRITE);
        srand(time(NULL));
        alarm(UPTIME);
        running = true;

	      printf("-- Server started --\n");

/* FORK INITIALIZATION & MAIN LOOP */

        pid = fork();
        if(pid < 0)
        {
                perror("fork");
                exit(EXIT_FAILURE);
        }

	      while(running)
	      {
		        if(pid == 0) /* Child Process */
		        {
                switch(cli())
                {
                    case 0:

                      stopServer(&running, pMap, getppid());
                      break;

                    case 1:

                      if(uATIS(pMap))
                        printf("ATIS updated\n");

                      else
                        printf("Error during ATIS update\n");
                      break;

                    case 2:
                      if(!pATIS(pMap))
                        printf("Meteo: Error during ATIS printing\n");
                      break;

                    default:

                      printf("Unrecognized command, try :\n");
		                  printf("exit : stop the server\n");
                      printf("uATIS : update database \n");
                      printf("pATIS : print database \n");
                      break;
                }

		       }

		       else /* Parent Process */
		       {
			          if(update == true)
			          {
				              if(uATIS(pMap))
				              {
					                   printf("INFO: ATIS updated\n");
				              } else {
					                   printf("INFO: Error during ATIS update\n");
				              }

				              printf("INFO: Next update in %d seconds\n", UPTIME);
				              alarm(UPTIME);
				              update = false;
			          }
		       }
	     }

	     return EXIT_SUCCESS;
}

/*
 * Command Line Interface to deal with user inputs, no
 * big programming skillz here. We just include the /n
 * in strcmp() as getline() includes it in the returned
 * string. Therefore allowing us to not deal with stdin buffer.
 */

int cli()
{
  unsigned short int cmdSize, returnValue;
	char *command = NULL;
	size_t len;

  cmdSize = 10;
  command = malloc(cmdSize*sizeof(char));

	if(getline(&command, &len, stdin) == -1)
  {
    perror("getline");
    return -1;
  }

  if(strcmp(command, "exit\n") == 0)
    returnValue = 0;

  else if(strcmp(command, "uATIS\n") == 0)
    returnValue = 1;

  else if(strcmp(command, "pATIS\n") == 0)
    returnValue = 2;

  else
    returnValue = -1;

  free(command);
  return returnValue;
}

/*
 * Simple function to print current ATIS state.
 */

bool pATIS(char *pMap)
{
	char atis[MSG_SIZE];
	int i;
	for(i = 0; i<AREAS; i++)
	{
		if(snprintf(atis, MSG_SIZE, "%s", pMap+(i*MSG_SIZE)) < 0)
    {
    	perror("sprintf");
      return false;
    }
		puts(atis);
	}

	return true;
}

/*
 * Fictionnal ATIS update from
 * random source row (0 < row < 6) at ATIS[][] to
 * random destination row (0 < row < 4) at pMap/
 * Gonna be moved to a dedicated function soon.
 */

bool uATIS(char *pMap)
{
        int dRow = rand() % 3;
        int sRow = rand() % 5;
        if(snprintf(pMap+(dRow)*MSG_SIZE, MSG_SIZE, "%s", ATIS[sRow]) < 0)
        {
        	perror("sprintf");
          return false;
        }

	return true;
}


/*
 * TODO
 * Prototype of the future alarm handler which's
 * gonna happen periodically (every UPTIME) to
 * update the ATIS mmap.
 */

void alrmHandler(int sig)
{
	update = true;
}

void stopServer(bool *running, char* pMap, int ppid)
{
	*running = false;
	munmap(pMap, sizeof(3 * MSG_SIZE));
  kill(ppid, SIGINT);
	printf("-- Server stopped --\n");
}
