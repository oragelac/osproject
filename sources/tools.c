#include "global.h"

/*
 * Generic server stop & clean function,
 * we set the running boolean to false for the
 * main loop, unmap the mmap.
 */

char* mmapFile(mode_t fileMode, signed int mapMode)
{
	int fd;
	struct stat fileStat;
	char *pMap;

	fd = open(FILENAME, fileMode);
	if(fd == -1)
	{
		perror("Error opening file : ");
		return NULL;
	}

	/* Returns information about the file to check its availibility */
	if(fstat(fd, &fileStat) < 0)
	{
		perror("fstat");
		close(fd);
		return NULL;
	}

	/* mmap the file and mark it as shared */
	pMap = (char*) mmap(NULL, sizeof(5*MSG_SIZE), mapMode, MAP_SHARED, fd, 0);
	if(pMap == MAP_FAILED)
	{
		perror("map");
		close(fd);
		return NULL;
	}

	close(fd);
	return pMap;
}
