#include "global.h"

typedef struct sockaddr_in sockaddr_in;

int main( int argc, char *argv[])
{
   int sockfd, newsockfd;
   unsigned int clientLength, port;
   char buffer[256];
   char *pMap;
   sockaddr_in serverAddress, clientAddress;
   pid_t pid;


   if(argc != 2)
   {
     fprintf(stderr,"usage %s port\n", argv[0]);
     exit(EXIT_FAILURE);
   }

   pMap = mmapFile(O_RDONLY, PROT_READ);
   port = atoi(argv[1]);
   sockfd = socket(AF_INET, SOCK_STREAM, 0);

   if (sockfd < 0)
   {
      perror("Error opening socket");
      exit(EXIT_FAILURE);
   }

   /* Initialize socket structure */
   memset(&serverAddress, 0, sizeof(serverAddress));
   serverAddress.sin_family = AF_INET;
   serverAddress.sin_addr.s_addr = INADDR_ANY;
   serverAddress.sin_port = htons(port);

   /* Now bind the host address using bind() call. */
   if (bind(sockfd, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0)
   {
      perror("Error on binding");
      exit(EXIT_FAILURE);
   }

   /* Listening for the clients, process will wait for the incoming connection */

   listen(sockfd, 5);
   clientLength = sizeof(clientAddress);

   srand(time(NULL));

   while (true)
   {
      newsockfd = accept(sockfd, (struct sockaddr *) &clientAddress, &clientLength);

      if (newsockfd < 0)
      {
        perror("Error on accept");
        exit(EXIT_FAILURE);
      }

      pid = fork();
      if(pid < 0)
      {
        perror("fork");
        exit(EXIT_FAILURE);
      }

      /* Child process */

      if (pid == 0)
      {
        signed int n;
        memset(buffer, '\0', strlen(buffer));
        n = read(newsockfd, buffer, 256);

        if (n < 0)
        {
          perror("ERROR reading to socket");
          exit(EXIT_FAILURE);
        }

        printf("Message received : %s\n", buffer);

        char atisString[MSG_SIZE];
        int r = rand() % 3;
        if(snprintf(atisString, MSG_SIZE, "%s", pMap+(r * MSG_SIZE)) < 0)
        {
          perror("snprintf");
          exit(EXIT_FAILURE);
        }

        do
        {
          write(newsockfd, atisString, MSG_SIZE);
          memset(buffer, 0, strlen(buffer));
          n = read(newsockfd, buffer, 256);

          if (n < 0)
          {
            perror("ERROR reading to socket");
            exit(EXIT_FAILURE);
          }
          printf("Message received : %s\n", buffer);
        } while(buffer[0] == 'N' && buffer[1] == 'A' && buffer[2] == 'K');

        close(newsockfd);
      }
  }

  close(sockfd);
  return EXIT_SUCCESS;
}
