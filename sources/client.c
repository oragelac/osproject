#include "global.h"

#define h_addr h_addr_list[0] /* for backward compatibility */

int main(int argc, char *argv[])
{
   int sockfd, portno, n;
   struct sockaddr_in serv_addr;
   struct hostent *server;

   char buffer[256];

   if (argc != 3)
   {
      fprintf(stderr, "usage %s hostname port\n", argv[0]);
      exit(EXIT_FAILURE);
   }

   portno = atoi(argv[2]);

   /* Create a socket point */
   sockfd = socket(AF_INET, SOCK_STREAM, 0);

   if (sockfd < 0)
   {
      perror("ERROR opening socket");
      exit(EXIT_FAILURE);
   }

   server = gethostbyname(argv[1]);

   if (server == NULL)
   {
      fprintf(stderr,"ERROR no such host\n");
      exit(EXIT_FAILURE);
   }

   memset(&serv_addr, 0, sizeof(serv_addr));
   serv_addr.sin_family = AF_INET;
   memcpy(&serv_addr.sin_addr.s_addr, server->h_addr, server->h_length);
   serv_addr.sin_port = htons(portno);

   /* Now connect to the server */
   if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
   {
      perror("ERROR connecting");
      exit(EXIT_FAILURE);
   }

   /* Now ask for a message from the user, this message
      * will be read by server
   */

   memset(buffer, 0, sizeof(buffer));
   snprintf(buffer, sizeof("ATIS REQUEST"), "%s", "ATIS REQUEST");
   printf("%s", buffer);

   /* Send message to the server */
   n = write(sockfd, buffer, strlen(buffer));

   if (n < 0)
   {
      perror("ERROR writing to socket");
      exit(EXIT_FAILURE);
   }

   do
   {
     memset(buffer, 0, sizeof(buffer));
     printf("Waiting response from server\n");
     n = read(sockfd, buffer, 256);
     printf("%d\n", n);
     if (n < 0)
     {
        perror("ERROR reading from socket");
        exit(EXIT_FAILURE);
     }

     printf("ATIS MESSAGE : %s\n",buffer);

     if(n != MSG_SIZE)
     {
        write(sockfd, "NAK", strlen("NAK"));
     }

     else
     {
        write(sockfd, "ACK", strlen("ACK"));
     }

   } while(n != MSG_SIZE);

   return EXIT_SUCCESS;
}
